# notifications project

This API sends SMS and makes call to given number with given message through Twilio.

## Configurations

This app using ENV variables for its configurations. It also supports dotenv `.env` file. You can
use [.env.example](.env.example) by copy it as `.env`

| Variable               | Description                        | Default               |
|------------------------|------------------------------------|----------------------	|
| PORT                   | The HTTP Port for this app         | 8080                  |
| TWILIO_FROM_NUMBER     | The origin number for SMS/Calls    | +15005550006          |
| TWILIO_ACCOUNT_SID     | Twilio Account SID                 | ....                  |
| TWILIO_AUTH_TOKEN      | Twilio Auth TOKEN                  | ....                  |

[Where are my test credentials?](https://support.twilio.com/hc/en-us/articles/223136147-Where-are-my-test-credentials-)

## Development Guide

Notes:

* Follow [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html). If you
  don't [checkstyle](https://checkstyle.sourceforge.io/) will fail.

* Run tests before committing changes. If you install the [pre-commit](https://pre-commit.com/#install)
  it will run tests before committing automatically.

* Create a branch for changes and create merge request to merging to master. Ask somebody to review.

* Keep commits small, keep merge requests small. Small as possible as it can.

### Run the App in dev mode

You can run your application in dev mode that enables live coding using:

```shell script
./mvnw compile quarkus:dev
```

You can also use [Swagger UI](http://localhost:8080/q/swagger-ui/)
