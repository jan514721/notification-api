package com.sample.notifications;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Getter
@Setter
public class MobileRequest {

  @NotBlank
  @Pattern(
      regexp = "^\\+?[1-9]\\d{1,14}$",
      message = "must be in E.164 format https://www.twilio.com/docs/glossary/what-e164"
  )
  @Schema(example = "+15005550010")
  private String phone;

  @NotBlank
  @Size(max = 1600)
  @Schema(example = "Hello")
  private String message;

}
