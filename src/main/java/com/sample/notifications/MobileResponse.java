package com.sample.notifications;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@RegisterForReflection
@Setter
@Getter
@AllArgsConstructor
public class MobileResponse {

  private String status;

}
