package com.sample.notifications;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.zalando.problem.ThrowableProblem;
import org.zalando.problem.violations.ConstraintViolationProblem;

@Path("/v2")
public class TwilioController {

  @ConfigProperty(name = "twilio.from.number")
  String fromNumber;

  @Inject
  TwilioService twilioService;

  /**
   * The Endpoint for sending SMS.
   *
   * @param request HTTP POST request body
   * @return HTTP response
   */
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/sms")
  @Operation(summary = "Sends SMS")
  @APIResponses(value = {
      @APIResponse(
          responseCode = "201",
          description = "Status of the SMS"
      ),
      @APIResponse(responseCode = "400", description = "Bad Request",
          content = @Content(
              mediaType = "application/problem+json",
              schema = @Schema(implementation = ConstraintViolationProblem.class)
          )
      ),
      @APIResponse(responseCode = "500", description = "Service Unavailable",
          content = @Content(
              mediaType = "application/problem+json",
              schema = @Schema(implementation = ThrowableProblem.class)
          )
      )
  })
  public Response sms(@Valid MobileRequest request) {
    var message = twilioService.sms(fromNumber, request.getPhone(), request.getMessage());

    var mobileResponse = new MobileResponse(message.getStatus().toString());

    return Response.ok(mobileResponse).status(Status.CREATED).build();
  }

  /**
   * The Endpoint for making Call.
   *
   * @param request HTTP POST request body
   * @return HTTP response
   */
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/call")
  @Operation(summary = "Makes a phone call")
  @APIResponses(value = {
      @APIResponse(
          responseCode = "201",
          description = "Status of the call"
      ),
      @APIResponse(responseCode = "400", description = "Bad Request",
          content = @Content(
              mediaType = "application/problem+json",
              schema = @Schema(implementation = ConstraintViolationProblem.class)
          )
      ),
      @APIResponse(responseCode = "500", description = "Service Unavailable",
          content = @Content(
              mediaType = "application/problem+json",
              schema = @Schema(implementation = ThrowableProblem.class)
          )
      )
  })
  public Response call(@Valid MobileRequest request) {
    var call = twilioService.call(fromNumber, request.getPhone(), request.getMessage());

    var mobileResponse = new MobileResponse(call.getStatus().toString());

    return Response.ok(mobileResponse).status(Status.CREATED).build();
  }
}
