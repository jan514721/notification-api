package com.sample.notifications;

import com.twilio.http.NetworkHttpClient;
import com.twilio.http.Request;
import com.twilio.http.TwilioRestClient;
import io.quarkus.arc.Unremovable;
import java.util.List;
import java.util.Map.Entry;
import javax.enterprise.context.Dependent;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.Produces;

@Dependent
class TwilioRestClientBuilder {

  @Unremovable
  @Singleton
  @Produces
  @Named("restClient")
  public TwilioRestClient restClient() {

    var wireMockUrl = System.getProperty("twilio.url");
    if (wireMockUrl == null) {
      return null;
    }

    var client = new NetworkHttpClient() {
      @Override
      public com.twilio.http.Response makeRequest(Request originalRequest) {
        var url = originalRequest.getUrl();

        var modified = new Request(originalRequest.getMethod(),
            url.replace("https://api.twilio.com", wireMockUrl));

        for (Entry<String, List<String>> entry : originalRequest.getPostParams().entrySet()) {
          entry.getValue().forEach(value -> modified.addPostParam(entry.getKey(), value));
        }

        modified.setAuth(originalRequest.getUsername(), originalRequest.getPassword());

        return super.makeRequest(modified);
      }
    };

    return new TwilioRestClient.Builder("username", "password")
        .accountSid("username")
        .httpClient(client)
        .build();
  }
}
