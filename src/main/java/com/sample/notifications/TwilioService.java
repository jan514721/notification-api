package com.sample.notifications;

import com.twilio.rest.api.v2010.account.Call;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.twiml.TwiML;
import com.twilio.twiml.VoiceResponse;
import com.twilio.twiml.voice.Pause;
import com.twilio.twiml.voice.Say;
import com.twilio.twiml.voice.Say.Voice;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.apache.camel.FluentProducerTemplate;
import lombok.extern.jbosslog.JBossLog;

@ApplicationScoped
@JBossLog
public class TwilioService {

  @Inject
  FluentProducerTemplate producerTemplate;

  private TwiML getTwimlFromMessage(String message) {

    var say = new Say.Builder(message).voice(Voice.ALICE).language(Say.Language.EN_GB).build();
    var pause = new Pause.Builder().length(1).build();
    return new VoiceResponse.Builder().pause(pause).say(say).build();

  }

  /**
   * SMS sender method.
   *
   * @param from        the phone number
   * @param to          the phone number
   * @param messageText message content
   * @return Message
   */
  public Message sms(String from, String to, String messageText) {
    log.infov("action: sms, from: {0}, to: {1}, messageText: {2}", from, to, messageText);
    Message message = producerTemplate.toF("twilio:message/creator?from=RAW(%s)&to=RAW(%s)&body=%s",
        from, to, messageText).request(Message.class);
    log.infov("Twilio response message status: {0}", message.getStatus());
    return message;

  }

  /**
   * Phone caller method.
   *
   * @param from        the phone number
   * @param to          the phone number
   * @param messageText message content
   * @return Call
   */
  public Call call(String from, String to, String messageText) {
    var twiml = getTwimlFromMessage(messageText);
    log.infov("action: call, from: {0}, to: {1}, messageText: {2}", from, to, messageText);
    Call call = producerTemplate.toF("twilio:call/creator?from=RAW(%s)&to=RAW(%s)&url=RAW(https://twimlets.com/echo?Twiml=%s)",
        from, to, twiml.toUrl()).request(Call.class);
    log.infov("Twilio response call status: {0}", call.getStatus());
    return call;
  }

}
