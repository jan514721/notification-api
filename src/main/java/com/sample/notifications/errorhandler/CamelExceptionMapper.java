package com.sample.notifications.errorhandler;

import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;

import com.twilio.exception.TwilioException;
import javax.inject.Singleton;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.camel.CamelExecutionException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;

@Provider
@Singleton
public class CamelExceptionMapper implements ExceptionMapper<CamelExecutionException> {

  @Override
  public Response toResponse(CamelExecutionException exception) {

    var httpStatus = Status.INTERNAL_SERVER_ERROR;

    var realException = ExceptionUtils.getThrowableList(exception).stream()
        .filter(TwilioException.class::isInstance)
        .findFirst()
        .orElse(exception.getCause());

    return Response.status(httpStatus.getStatusCode())
        .type("application/problem+json")
        .header(CONTENT_TYPE, "application/problem+json")
        .entity(Problem.valueOf(httpStatus, realException.getMessage()))
        .build();
  }
}
