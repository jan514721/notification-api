package com.sample.notifications.errorhandler;

import java.util.stream.Collectors;
import javax.inject.Singleton;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.zalando.problem.Status;
import org.zalando.problem.violations.ConstraintViolationProblem;
import org.zalando.problem.violations.Violation;

@Provider
@Singleton
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

  @Override
  public Response toResponse(ConstraintViolationException exception) {

    var violations =
        exception.getConstraintViolations().stream()
            .map(c -> new Violation(c.getPropertyPath().toString(), c.getMessage()))
            .collect(Collectors.toList());

    var httpStatus = Status.BAD_REQUEST;

    return Response.status(httpStatus.getStatusCode())
        .type("application/problem+json")
        .entity(new ConstraintViolationProblem(httpStatus, violations))
        .build();
  }
}
