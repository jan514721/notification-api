package com.sample.notifications.errorhandler;

import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;

import com.fasterxml.jackson.core.JsonProcessingException;
import javax.inject.Singleton;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;

@Provider
@Singleton
public class JsonProcessingExceptionMapper implements ExceptionMapper<JsonProcessingException> {

  @Override
  public Response toResponse(JsonProcessingException exception) {

    var httpStatus = Status.BAD_REQUEST;

    return Response.status(httpStatus.getStatusCode())
        .type("application/problem+json")
        .header(CONTENT_TYPE, "application/problem+json")
        .entity(Problem.valueOf(httpStatus, exception.getMessage()))
        .build();
  }
}
