package com.sample.notifications.errorhandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.quarkus.jackson.ObjectMapperCustomizer;
import javax.inject.Singleton;

@SuppressFBWarnings(value = "EQ_COMPARETO_USE_OBJECT_EQUALS", justification = "This problem belongs to Quarkus Jackson")
@Singleton
public class ProblemObjectMapperCustomizer implements ObjectMapperCustomizer {

  public void customize(ObjectMapper mapper) {
    mapper.findAndRegisterModules();
  }

}
