package com.sample.notifications.it;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import java.net.HttpURLConnection;
import javax.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;

@QuarkusTestResource(TwilioTestResourceManager.class)
@QuarkusTest
class ResourceTest {

  @Test
  void sendMessage() {
    given()
        .body("{\"phone\":\"+15005550010\",\"message\":\"test\"}")
        .contentType(MediaType.APPLICATION_JSON).when().post("/v2/sms")
        .then()
        .statusCode(HttpURLConnection.HTTP_CREATED)
        .contentType(MediaType.APPLICATION_JSON)
        .body("status", is("queued"));
  }

  @Test
  void makeCall() {
    given()
        .body("{\"phone\":\"+15005550010\",\"message\":\"test\"}")
        .contentType(MediaType.APPLICATION_JSON).when().post("/v2/call")
        .then()
        .statusCode(HttpURLConnection.HTTP_CREATED)
        .contentType(MediaType.APPLICATION_JSON)
        .body("status", is("queued"));
  }

  @Test
  void sendMessageInvalidJsonRequest() {
    given()
        .body("{")
        .contentType(MediaType.APPLICATION_JSON).when().post("/v2/sms")
        .then()
        .statusCode(HttpURLConnection.HTTP_BAD_REQUEST)
        .contentType("application/problem+json");
  }

  @Test
  void sendMessageEmptyMessage() {
    given()
        .body("{\"phone\":\"+15005550010\",\"message\":\"\"}")
        .contentType(MediaType.APPLICATION_JSON).when().post("/v2/sms")
        .then()
        .statusCode(HttpURLConnection.HTTP_BAD_REQUEST)
        .contentType("application/problem+json")
        .body("violations[0].field", containsString("message"))
        .body("violations[0].message", is("must not be blank"));
  }

  @Test
  void sendMessageEmptyPhone() {
    given()
        .body("{\"phone\":\"\",\"message\":\"test\"}")
        .contentType(MediaType.APPLICATION_JSON).when().post("/v2/sms")
        .then()
        .statusCode(HttpURLConnection.HTTP_BAD_REQUEST)
        .contentType("application/problem+json")
        .body("violations.message", hasItems(is("must not be blank")));
  }

  @Test
  void sendMessageInvalidFormatPhone() {
    given()
        .body("{\"phone\":\"++123\",\"message\":\"test\"}")
        .contentType(MediaType.APPLICATION_JSON).when().post("/v2/sms")
        .then()
        .statusCode(HttpURLConnection.HTTP_BAD_REQUEST)
        .contentType("application/problem+json")
        .body("violations[0].message", containsString("must be in E.164 format"));
  }

  @Test
  void sendMessageToInvalidNumber() {
    given()
        .body("{\"phone\":\"+15005550001\",\"message\":\"test\"}")
        .contentType(MediaType.APPLICATION_JSON).when().post("/v2/sms")
        .then()
        .statusCode(HttpURLConnection.HTTP_INTERNAL_ERROR)
        .contentType("application/problem+json")
        .body("detail", containsString("is not a valid phone number"));
  }

  @Test
  void sendMessageToNonMobileNumber() {
    given()
        .body("{\"phone\":\"+15005550009\",\"message\":\"test\"}")
        .contentType(MediaType.APPLICATION_JSON).when().post("/v2/sms")
        .then()
        .statusCode(HttpURLConnection.HTTP_INTERNAL_ERROR)
        .contentType("application/problem+json")
        .body("detail", containsString("is not a mobile number"));
  }


  @Test
  void sendMessageToBlacklistedNumber() {
    given()
        .body("{\"phone\":\"+15005550004\",\"message\":\"test\"}")
        .contentType(MediaType.APPLICATION_JSON).when().post("/v2/sms")
        .then()
        .statusCode(HttpURLConnection.HTTP_INTERNAL_ERROR)
        .contentType("application/problem+json")
        .body("detail", containsString("violates a blacklist rule."));
  }

}
