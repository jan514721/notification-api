package com.sample.notifications.it;

import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

import com.github.tomakehurst.wiremock.WireMockServer;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import java.util.Collections;
import java.util.Map;

public class TwilioTestResourceManager implements QuarkusTestResourceLifecycleManager {

  private WireMockServer wireMockServer;

  @Override
  public Map<String, String> start() {

    var username = System.getenv("TWILIO_ACCOUNT_SID");
    var password = System.getenv("TWILIO_AUTH_TOKEN");

    if (username != null && password != null) {
      return Collections.emptyMap();
    }

    wireMockServer = new WireMockServer(options().dynamicPort().usingFilesUnderDirectory("src/test/resources/twilio"));
    wireMockServer.start();

    configureFor(wireMockServer.port());

    return Collections.singletonMap("twilio.url", wireMockServer.baseUrl());
  }

  @Override
  public void stop() {
    if (null != wireMockServer) {
      wireMockServer.stop();
    }
  }
}
